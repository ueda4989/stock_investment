# pythonを実行できる状態にする方法
## pyenv install (pythonバージョン管理)
```
sudo apt update && sudo apt upgrade
git clone https://github.com/yyuu/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
source ~/.bashrc
pyenv
sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev
sudo apt-get install libffi-dev
pyenv install [バージョン] # ex) 3.8.0
pyenv global [バージョン]
```

## pip install (pythonパッケージ管理)
```
pip install --upgrade pip
pip install pipenv
```

## 確認
```
python -V
pip -V
```

# Postgresqlチューニング
EDINETより取得したデータをテーブルに挿入後、データ抽出をしたが遅すぎるため改善策を検討してみた。

## チェックポイントの発生周期が短いらしいので、チューニングしてみた。　以下、一部抜粋
```
$ sudo cat /var/log/postgresql/postgresql-11-main.log 
2019-12-01 17:41:46.969 JST [501] LOG:  チェックポイントの発生周期が短すぎます(28秒間隔)
2019-12-01 17:41:46.969 JST [501] ヒント:  設定パラメータ"max_wal_size"を増やすことを検討してください
```
## 設定ファイル修正
### 参考URL
[PostgreSQLのチェックポイント処理のチューニング](https://qiita.com/U_ikki/items/89b1eea657e47120e3ee)  
翻訳元サイト  
[Basics of Tuning Checkpoints](https://www.2ndquadrant.com/en/blog/basics-of-tuning-checkpoints/)  
postgresql.confの各種設定の詳細  
[19.5。 先行ログを書き込む](https://code-examples.net/ja/docs/postgresql~10/runtime-config-wal)

### 以下、修正項目
* checkpoint_timeout ※デフォルト : 5min
* max_wal_size ※デフォルト : 1GB
* checkpoint_completion_target ※デフォルト : 0.5 

```
$ sudo # - Checkpoints -

#checkpoint_timeout = 5min              # range 30s-1d
checkpoint_timeout = 60min
max_wal_size = 60GB # postgresのデフォルト値が低すぎる。。。
min_wal_size = 80MB
#checkpoint_completion_target = 0.5     # checkpoint target duration, 0.0 - 1.0
checkpoint_completion_target = 0.96
#checkpoint_flush_after = 256kB         # measured in pages, 0 disables
#checkpoint_warning = 30s               # 0 disables

```

## 結果
各段に早くなった。※体感速度(笑)
WALへの書き込みサイズが影響していたことは間違いなかったようだ。
いかんせんデータ件数が800万を超えており、今後も年単位で増えることは目に見えているので、
とりあえずは良しとする。  

また別要因で遅くなる場合は、パラレルクエリやテーブルのパーテーションも検討する必要あり
あーめんどくせー
