# -*- coding: utf-8 -*-
import notification  # 通知用モジュール呼び出し
import traceback
from termcolor import colored, cprint

# TODO エラーハンドリング


def except_error(e):  # todo error処理(とりあえず共通化)
    # エラー通知
    cprint(e, 'red')  # エラーメッセージ
    cprint(type(e), 'red')  # エラータイプ
    cprint(traceback.print_exc(), 'white', 'on_red')  # トレース
    cprint('エラーが発生したため、処理を終了します。', 'red')
    exit()
