# -*- coding: utf-8 -*-
from termcolor import colored, cprint  # https://pypi.org/project/termcolor/

# TODO オリジナルの通知設定や色の設定が可能(同じディレクトリならパス指定不要)

# 使えるテキストの色
# grey
# red
# green
# yellow
# blue
# magenta
# cyan
# white

# 使える背景色
# on_grey
# on_red
# on_green
# on_yellow
# on_blue
# on_magenta
# on_cyan
# on_white

# attrs [attrs=以下より指定]
# bold
# dark
# underline
# blink
# reverse
# concealed


def title_success(message=''):
    # 成功
    title = '-' * 30 + '\n'
    title += '|{:^28}|\n'.format('通知')
    title += '-' * 30 + '\n\n'

    result = message_empty_check(title, message)

    return colored(result, 'green')


def title_caution(message=''):
    # 注意
    title = '-' * 30 + '\n'
    title += '|{:^28}|\n'.format('注意')
    title += '-' * 30 + '\n\n'

    result = message_empty_check(title, message)

    return colored(result, 'yellow')


def title_error(message=''):
    # エラー
    title = '*' * 30 + '\n'
    title += '*{:^26}*\n'.format('エラー')
    title += '*' * 30 + '\n\n'

    result = message_empty_check(title, message)

    return colored(result, 'red')


def message_empty_check(title, message):
    # メッセージあれば追加
    if message:
        title += message + '\n'
        return title
