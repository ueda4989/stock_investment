#!/usr/bin/env python3

import sys
import os


def is_empty(directory):
    files = os.listdir(directory)  # 返却値：配列
    files = [f for f in files
             if not f.startswith(".")]  # 返却値：配列
    if files:
        return True
    else:
        print(directory + ' のディレクトリの中身は空です。')
        return False


if __name__ == '__main__':  # 単独実行用
    src = sys.argv[1]  # 引数受け取り
    is_empty(src)
