# coding: utf-8

import os
import re
import csv
import sys
import notification  # 通知用モジュール(自作)
import errorhandle  # エラーハンドリングモジュール(自作)
from xbrl import XBRLParser
from collections import defaultdict
from termcolor import colored, cprint

args = sys.argv  # コマンドライン引数

default_tag = ['file_nm', 'element_id', 'amount']
custom_tag = ['unit_ref', 'decimals', 'contextref']
year_tag = ['year']  # 収集した年を区別する為
encode_type = 'utf-8'


class XbrlParser(XBRLParser):
    def __init__(self, xbrl_filepath):
        self.xbrl_filepath = xbrl_filepath

    def parse_xbrl(self, year):
        # parse xbrl file
        with open(self.xbrl_filepath, 'r', encoding='utf-8') as of:
            xbrl = XBRLParser.parse(of)  # beautiful soup type object
        result_dicts = defaultdict(list)
        _idx = 0

        # print xbrl
        name_space = 'jp*'
        for node in xbrl.find_all(name=re.compile(name_space+':*')):
            if self.ignore_pattern(node):
                continue

            row_dict = defaultdict(list)
            # default tag
            row_dict['file_nm'] = self.xbrl_filepath.rsplit(os.sep, 1)[1]
            row_dict['element_id'] = node.name
            row_dict['amount'] = node.string

            # cutom tag
            for tag in custom_tag:
                row_dict[tag] = self.get_attrib_value(node, tag)

            # year tag
            row_dict['year'] = year + '-12-31'

            result_dicts[_idx] = row_dict
            _idx += 1
        return result_dicts

    def ignore_pattern(self, node):
        if 'xsi:nil' in node.attrs:
            if node.attrs['xsi:nil'] == 'true':
                return True
        # 結果が空の場合は対象外にする
        if not isinstance(node.string, str):
            return True
        # 改行コードが結果にある場合対象外にする
        if str(node.string).find(u'\n') > -1:
            return True
        # text文字列は解析に利用できないため、対象外
        if u'textblock' in str(node.name):
            return True
        return False

    def get_attrib_value(self, node, attrib):
        if attrib in node.attrs.keys():
            return node.attrs[attrib]
        else:
            return None

# ディレクトリ配下のファイルのうちis_xbrl_fileがTrueのもののみ取得する


def make_directory(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)


def fild_all_files(directory):
    for root, dirs, files in os.walk(directory):
        print(dirs)  # 警告を回避
        for file in files:
            if not is_xbrl_file(root, file):
                continue
            yield os.path.join(root, file)


def is_xbrl_file(root_path, file_name):
    # xbrlファイルでなければ対象外
    if not file_name.endswith('.xbrl'):
        return False
    # AuditDocは対象外
    if u'AuditDoc' in str(root_path):
        return False
    return True


def dump_file(writer, info_dicts):
    _idx = 0
    while _idx < len(info_dicts):
        row_dict = info_dicts[_idx]
        writer.writerow(row_dict)
        _idx += 1


def main(year, output_file):
    # 「xbrl_files_yyyy」フォルダを検索し、フルパス取得
    base_path = os.getcwd() + '/xbrl_files_' + year

    # writerオブジェクト生成
    resultCsvWriter = csv.DictWriter(
        open(output_file, 'w', encoding=encode_type),   default_tag + custom_tag + year_tag,   lineterminator="\n")
    resultCsvWriter.writeheader()

    # 対象ファイルを抽出
    xbrl_files = fild_all_files(base_path)
    count = 0
    for xbrl_file in xbrl_files:
        count += 1
        cprint('getting data...', 'green', attrs=['bold'])
        print(xbrl_file)
        xp = XbrlParser(xbrl_file)
        info_dicts = xp.parse_xbrl(year)
        dump_file(resultCsvWriter, info_dicts)


if __name__ == '__main__':
    if len(args) == 2:  # 第一引数が存在するか?
        year = args[1]  # データ取得する年(yyyy)
        output_file = 'egg_' + year + '.csv'
        csv_dir = './csv/'
        output_file_path = csv_dir + output_file
        # 出力ディレクトリ作成
        make_directory(csv_dir)
    else:
        # エラー通知
        print(notification.title_error('第一引数に取得する年を数字4桁で入力してください'))
        print('$ 3rd_step_csv.py <year>')
        quit()

    # 日付設定確認
    print('year:', year)

    # 処理実行
    main(year, output_file_path)

    # 通知
    print(notification.title_success('CSV変換成功!!'))
