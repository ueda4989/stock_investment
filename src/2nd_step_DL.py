# -*- coding: utf-8 -*-
import requests
import json
import os
import io
import shutil
import sys
import multiprocessing
import traceback
import random
import time
# https://pypi.org/project/termcolor/ colored : 変数に渡す。cprint : print()同様そのまま出力可
from termcolor import colored, cprint
import notification  # 通知モジュール(自作)
import fileanddirectorycheck as fanddcheck  # フォルダの中身チェック(自作)
import errorhandle  # エラーハンドリングモジュール(自作)
from collections import defaultdict
from zipfile import ZipFile

args = sys.argv  # コマンドライン引数


def make_directory(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)

# is_yuho関数に当てはまる全ての企業×有報情報IDごとに取得する


def _download_xbrl_file(info_dicts, directory_path):
    # ディクショナリ初期化
    mp_dict = defaultdict(dict)
    counter = 0

    # タイムアウト設定
    connect_time = 10.0  # 10秒
    read_time = 20.0  # 20秒

    for _id, info_dict in info_dicts.items():
        # jsonファイル内ループ
        mp_dict[counter] = info_dict
        counter += 1

        # 証券CDごとのディレクトリ作成
        company_path = directory_path + info_dict['cd'] + '/'
        ir_path = company_path + info_dict['id']
        make_directory(company_path)
        #　証券コード×IR情報ごとのディレクトリ作成
        if os.path.exists(ir_path) and fanddcheck.is_empty(ir_path):
            print('既に同一フォルダが存在するためスキップします。' + ir_path)
            continue
        make_directory(ir_path)
        cprint('DLファイルパス : ' + ir_path, 'white', attrs=['underline'])

        url = info_dict['url']
        count, retry = 0, 3  # count(リトライ数)、retry(最大リトライ数)
        wait_time = random.randint(500, 1000)  # 少し間を開けてリトライ
        # 時間をずらす
        second_time = random.randint(2, 5)  # 不規則な時間
        cprint(str(second_time) + ' 秒後にアクセス実行',
               'blue', attrs=['bold'])
        time.sleep(second_time)  # アクセス過多を防ぐため

        # request
        cprint('ダウンロード中...', 'green')
        while True:
            try:
                # Requestによりファイルを取得して、Unzipする
                r = requests.get(url, timeout=(connect_time, read_time))
                z = ZipFile(io.BytesIO(r.content))
                z.extractall(ir_path)  # unzip the file and save files to path.
                break  # 作成後、次の情報取得

            except requests.exceptions.ConnectTimeout as e:
                # リトライ処理
                print(colored('リクエストに失敗しました。リトライします。', 'yellow'))
                if count < retry:
                    time.sleep(wait_time * random.random())  # 待機
                    count += 1
                    continue
                else:
                    notification.title_error(
                        'リトライを3回実行しましたが、ダウンロードに失敗しました。処理を終了します。')
                    raise errorhandle.except_error(r.status_code)
            else:
                notification.title_error('リクエストエラー発生。処理を終了します。')
                cprint(r.status_code, 'red')
                cprint(r.json(), 'red')
                exit()


def download_json_url(year):
    search_directory = os.getcwd() + '/downloaded_info_' + year + '/'
    old_directory = search_directory + 'old/'
    make_directory(old_directory)
    # xbrl_filesのディレクトリが存在しないとき、作成する
    directory_path = os.getcwd() + '/xbrl_files_' + year + '/'
    make_directory(directory_path)

    for file_name in os.listdir(search_directory):

        if not u'.json' in file_name:
            continue
        print(file_name + ' loading...')
        with open(search_directory + file_name, 'r') as of:
            info_dict = json.load(of)

        _download_xbrl_file(info_dict, directory_path)
        shutil.move(search_directory + file_name, old_directory + file_name)


if __name__ == '__main__':
    if len(args) == 2:  # 第一引数が存在するか?
        year = args[1]  # データ取得する年(yyyy)
    else:
        # エラー通知
        print(notification.title_error('第一引数に取得する年を数字4桁で入力してください'))
        print('$ 2nd_step_DL.py <year>')
        quit()

    # 日付設定
    print('year:', year)

    # 処理実行
    download_json_url(year)

    # 通知
    print(notification.title_success('ダウンロード成功!!'))
