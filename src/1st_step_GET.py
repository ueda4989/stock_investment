# -*- coding: utf-8 -*-
import requests  # pip install requests
import xml.etree.ElementTree as ET
import json
import os
import io
import re
import multiprocessing
import sys
import pandas as pd  # pip install pandas
import time
import random
import traceback
import shutil
from collections import defaultdict
from datetime import datetime as dt

args = sys.argv  # コマンドライン引数
csvEdinetCode = 'EdinetcodeDlInfo.csv'  # EDINETコードのcsvファイル
messageCopy = 'コピー先に作業中のCSVファイルが存在します。最初からやり直しますか？'
# pandasの出力フォーマットを整数に変換
pd.options.display.float_format = '{:.0f}'.format

# todo: 最終更新日(updated)が今年分しか無いため1か月単位でcron実行したい
# todo: xbrlのフォルダをcron実行した年月の名前にして、そのフォルダが存在する場合は次の月のフォルダを作成 -> xbrlのDL -> csv作成
# todo: csvファイルは、一か月単位で保持し、別で蓄積したcsvファイルを作成 -> 差分を追加する みたいなことができればいいな


def get_link_info_str(ticker_symbol, base_url):
    # http://resource.ufocatch.com/atom/edinetx/ [ edinet code]
    url = base_url + ticker_symbol
    # url = base_url + '38610'
    response = requests.get(url)
    print('url :' + url)
    return response.text


def get_link(tree, namespace, start_date, end_date):
    yuho_dict = defaultdict(dict)
    print('yuho_dict: ' + str(yuho_dict))
    try:
        print('tree :' + str(tree.tag))
        print(str('.//' + namespace + 'entry'))
        # entryタグ毎にforeach
        for el in tree.findall(str('.//' + namespace + 'entry')):
            """
            titleタグに
            有価証券報告書の文字が有るかつ
            内国投資信託受益証券の文字が無ければ
            後続処理を実行
            """
            title = el.find(namespace + 'title').text  # titleタグ検索
            if not is_yuho(title):
                continue

            updated = el.find(namespace + 'updated').text  # updatedタグ検索
            checked = time_check(updated, start_date, end_date)
            print('title: ' + title)
            print('updated: ' + str(updated))
            print('checked: ' + str(checked))
            # 両方の条件を満たしたときに値を返す(1年に1回の更新ならではの処理)
            if checked['start_date'] and checked['end_date']:

                # zipファイルのアドレスを辞書オブジェクトへ登録
                _id = el.find(namespace+'id').text
                link = el.find('./'+namespace+'link[@type="application/zip"]')
                url = link.attrib['href']
                cd = re.sub(r'^【(\w+)】.*', r"\1", title)
                yuho_dict[_id] = {'id': _id, 'title': title,
                                  'cd': cd, 'url': url, 'update': updated}
                # 全銘柄全ての行を検索する。

        # 取得データ
        return yuho_dict

    except Exception as e:
        except_error(e)

# u''は、''内の文字列をunicode文字列として扱う。行末尾の\で改行が許される。
# 文字列検索モジュール


def is_yuho(title):
    if u'有価証券報告書' in str(title) \
            and u'内国投資信託受益証券' not in str(title):
                # and u'訂正有価証券報告書' not in str(title):  # 一旦外す
        return True
    else:
        return False


def time_check(update, start_date, end_date):
    # 期間終了日付内であれば更新日付を返す
    updated_time = dt.strptime(update, '%Y-%m-%dT%H:%M:%S+09:00')
    return {'start_date': updated_time >= start_date, 'end_date': updated_time < end_date}


def make_directory(dir_name):
    if not os.path.exists(dir_name):
        os.mkdir(dir_name)


def write_download_info(ofname, info_dict):
    with open(ofname, 'w') as of:
        json.dump(info_dict, of, indent=4)


def craete_xbrl_url_json(start_date, end_date, year, csv_file):
    # todo: namespaceをparseした際、なぜかhttpsがhttpになってしまう。秘匿性を考えるとhttpはあまりよろしくない。
    # 有報キャッチャーWebServiceのAtomAPIアドレス<http://resource.ufocatch.com/>
    base_url = 'http://resource.ufocatch.com/atom/edinetx/query/'
    namespace = '{http://www.w3.org/2005/Atom}'

    # csvファイル読み込み
    csv_file = pd.read_csv("./" + csvCopyTarget, header=None, index_col=None,
                           encoding="cp932")
    csv_file = csv_file.dropna(how='all')  # 欠損値を除外(NaN)　これでかなり圧縮
    csv_file = csv_file.astype(int)  # 値をint型に変換
    csv_file = csv_file.reset_index(drop=True)  # index振り直し(念のため)

    for row in csv_file.itertuples():  # 「itertuples」で、行のインデックスと要素が取得できる

        # csvファイル更新
        csv_update(csv_file, row.Index)

        count, retry = 0, 3  # count(リトライ数)、retry(最大リトライ数)
        wait_time = random.randint(500, 1000)  # 少し間を開けてリトライ

        # 文字列変換
        t_symbol = str(row[1])  # page数ではなく、証券コードをquery/の後に追加

        # 時間をずらす
        second_time = random.randint(3, 10)  # 不規則な時間
        print(str(second_time) + '秒後にアクセス実行')
        time.sleep(second_time)  # アクセス過多を防ぐため

        print('証券コード:' + t_symbol + ', loading...')

        while True:
            try:
                # testの時は、コメントアウト start
                # 企業毎の有報へのデータへのリンク情報を取得
                response_string = get_link_info_str(t_symbol, base_url)

                # xmlをparseするElementTreeを取得(たまにxml.etree.ElementTree.ParseError: unclosed token:が発生するので、リトライで成功してほしい。)
                ET_tree = ET.fromstring(response_string)
                ET.register_namespace('', namespace[1:-1])  # この処理は？？

                # downloadファイルの対象を取得
                info_dict = get_link(
                    ET_tree, namespace, start_date, end_date)
                print('info_dict: ' + str(info_dict))
                # データが存在するか？
                if info_dict:
                    # Request用のJson形式のファイルを作成
                    json_directory = os.getcwd() + '/downloaded_info_' + year
                    make_directory(json_directory)  # ディレクトリ作成
                    ofname = json_directory+'/dat_download_'+t_symbol+'.json'
                    write_download_info(ofname, info_dict)
                    print(ofname + '\n作成完了!!')
                # testの時は、コメントアウト end
                break  # 作成後、次の情報取得

            except Exception as e:
                # リトライ処理
                print(
                    f'{t_symbol}のperseに失敗しました。[{count}] \n {response_string}')
                if count < retry:
                    time.sleep(wait_time * random.random())
                    count += 1
                    continue
                else:
                    raise except_error('リトライを3回実行しましたが、失敗しました。')

    # 取得データがなくなり次第、処理終了
    print('ダウンロード完了!!')

# 証券コードのみのCSVファイル作成


def csv_processing(csvCopySource, csvCopyTarget):
    try:
        # csvファイル読み込み(証券コード列のみ)
        csv_file = pd.read_csv(csvCopySource, skiprows=1,
                               usecols=['証券コード'], encoding="cp932")
        # 欠損値を除外(NaN)
        csv_file = csv_file.dropna(how='all')  # これでかなり圧縮される！

        # ファイル出力
        csv_file.to_csv(csvCopyTarget, header=False, index=False,
                        encoding='cp932')

    except PermissionError as pe:
        print('【出力エラー】「' + csvCopyTarget + '」を開いている場合は、閉じてから再実行してください。')
        print(pe)
        exit()


def csv_update(csv_file, index):  # 作業終了した証券コードを削除
    try:
        # 取得した証券コードの行を削除(1週目は、スルー)
        csv_update_file = csv_file.drop(range(0, index))
        # print(csv_update_file.head(10))  # 先頭10行取得
        # 削除した証券コード以外出力(これで再度実行したときは、作業した証券コード以外から取得可能)
        csv_update_file.to_csv(csvCopyTarget, header=False,
                               index=False, encoding='cp932')
    except PermissionError as pe:
        print('【出力エラー】「' + csvCopyTarget + '」を開いている場合は、閉じてから再実行してください。')
        print(pe)
        exit()


def yes_no_input(message):
    while True:
        choice = input(message + " 'yes' or 'no' [y/N]: ").lower()
        if choice in ['y', 'ye', 'yes']:
            return True
        elif choice in ['n', 'no']:
            return False


def except_error(e):  # error処理(とりあえず共通化)
    print(e)  # エラーメッセージ
    print(type(e))  # エラータイプ
    traceback.print_exc()  # トレース
    print('エラーが発生したため、処理を終了します。')
    quit()


if __name__ == '__main__':

    if len(args) == 2:  # 第一引数が存在するか?
        year = args[1]  # データ取得する年(yyyy)
    else:
        print('第一引数に取得する年を数字4桁で入力してください')
        print('$ 1st_step_GET.py <year>')
        quit()

    # 日付設定
    start_date = dt.strptime(year + '-01-01', '%Y-%m-%d')
    end_date = dt.strptime(year + '-12-31', '%Y-%m-%d')
    print('start_date:', start_date)
    print('end_date:', end_date)

    # pass check
    csvCopySource = './edinet_code/' + csvEdinetCode
    csvCopyTarget = './' + \
        os.path.splitext(csvEdinetCode)[0] + 'RowOnly' + \
        year + os.path.splitext(csvEdinetCode)[1]  # 各年ごとに作業ファイル作成
    if os.path.exists(csvCopySource) and os.path.isfile(csvCopySource):  # コピー元にファイルが存在する場合
        if os.path.exists(csvCopyTarget) and os.path.isfile(csvCopyTarget):  # コピー先にファイルが存在する場合
            if yes_no_input(messageCopy):  # 確認
                csv_processing(csvCopySource, csvCopyTarget)  # 再作成(上書き)
            else:
                print('続きから実行します')
        else:
            csv_processing(csvCopySource, csvCopyTarget)  # ファイルが無ければ作成
    else:
        except_error('csvファイルが存在しません')

    # 一定期間に発生したデータ全てのxbrlURLを取得する
    craete_xbrl_url_json(start_date, end_date, year, csvCopyTarget)
