-- egg_[西暦年].csv格納用テーブル(蓄積)
CREATE DATABASE stock_investment;

CREATE TABLE tb_egg (
    id bigserial PRIMARY KEY,
    file_nm text,
    element_id text,
    amount text,
    unit_ref text,
    decimals smallint,
    context_ref text,
    year_end date
);

CREATE INDEX ON tb_egg (year_end, element_id);

COMMENT ON TABLE tb_egg IS '株式情報テーブル (卵)';

COMMENT ON COLUMN tb_egg.file_nm IS 'ファイル名 (xbrlファイル名)';

COMMENT ON COLUMN tb_egg.element_id IS 'タグ名 (純利益等の項目名。詳細はタクソノミ参照)';

COMMENT ON COLUMN tb_egg.amount IS '金額 (他ドキュメントだと文字列が入る可能性あり)';

COMMENT ON COLUMN tb_egg.unit_ref IS '連結有無 (連結・単体)';

COMMENT ON COLUMN tb_egg.decimals IS '小数 (会計上表示するための単位)';

COMMENT ON COLUMN tb_egg.context_ref IS '時系列 (今年から最大4年前まで)';

COMMENT ON COLUMN tb_egg.year_end IS '取得日付 (年単位で区切るため)';

-- csvインポート start
COPY tb_egg (file_nm,
    element_id,
    amount,
    unit_ref,
    decimals,
    context_ref,
    year_end)
FROM
    '/mnt/c/developments/Sites/stock_investment/csv/egg_2016.csv' WITH csv header;

COPY tb_egg (file_nm,
    element_id,
    amount,
    unit_ref,
    decimals,
    context_ref,
    year_end)
FROM
    '/mnt/c/developments/Sites/stock_investment/csv/egg_2017.csv' WITH csv header;

COPY tb_egg (file_nm,
    element_id,
    amount,
    unit_ref,
    decimals,
    context_ref,
    year_end)
FROM
    '/mnt/c/developments/Sites/stock_investment/csv/egg_2018.csv' WITH csv header;

COPY tb_egg (file_nm,
    element_id,
    amount,
    unit_ref,
    decimals,
    context_ref,
    year_end)
FROM
    '/mnt/c/developments/Sites/stock_investment/csv/egg_2019.csv' WITH csv header;

-- csvインポート end
-- TODO マスターテーブルを作ろう
-- 区切り文字で列分割

DROP TABLE tb_egg_processing;

CREATE TABLE tb_egg_processing (
    id bigint PRIMARY KEY,
    target_document text,
    report_abbreviation text,
    report_sequence text,
    edinet_code text,
    sequence_number text,
    report_target_period_last_day date,
    report_submission_times text,
    submission_date date,
    file_extension text,
    name_space text,
    taxonomy_element_name text
);

CREATE INDEX ON tb_egg_processing (edinet_code, report_abbreviation, name_space, taxonomy_element_name);

COMMENT ON TABLE tb_egg_processing IS '株式情報テーブル (タグ情報分割)';

COMMENT ON COLUMN tb_egg_processing.target_document IS '対象書類';

COMMENT ON COLUMN tb_egg_processing.report_abbreviation IS '報告書略号';

COMMENT ON COLUMN tb_egg_processing.report_sequence IS '報告書連番(3桁)';

COMMENT ON COLUMN tb_egg_processing.edinet_code IS 'EDINETコード';

COMMENT ON COLUMN tb_egg_processing.sequence_number IS '追番';

COMMENT ON COLUMN tb_egg_processing.report_target_period_last_day IS '報告対象期末日';

COMMENT ON COLUMN tb_egg_processing.report_submission_times IS '報告書提出回数';

COMMENT ON COLUMN tb_egg_processing.submission_date IS '提出日';

COMMENT ON COLUMN tb_egg_processing.file_extension IS 'ファイル拡張子';

COMMENT ON COLUMN tb_egg_processing.name_space IS '名前空間';

COMMENT ON COLUMN tb_egg_processing.taxonomy_element_name IS 'タクソノミ要素名';

-- todo 先頭に「jpfr」とついたファイル名は、過去のタクソノミ規約に基づいたファイル名となっている
-- todo したがって、先頭ファイル名別で文字列分割の形式を変える必要あり。
-- file_nmがjpfr以外のデータのみ

INSERT INTO tb_egg_processing WITH st1 (
    id,
    a1,
    a2
) AS (
    -- 区切り文字で配列として分割
    SELECT
        id,
        regexp_split_to_array(file_nm, '_') AS c1,
        regexp_split_to_array(element_id, ':') AS c2
    FROM
        tb_egg
    WHERE
        file_nm NOT LIKE 'jpfr%' -- 旧EDINETのタクソノミを除く
),
st2 (
    id, a1, a2, c3, c4, a3, c6, c7
) AS (
    SELECT
        st1.id,
        regexp_split_to_array(a1[1], '-') AS c1,
        regexp_split_to_array(a1[2], '-') AS c2,
        a1[3] AS c3,
        a1[4] AS c4,
        regexp_split_to_array(a1[5], '\.') AS c5,
        a2[1] AS c6,
        a2[2] AS c7
    FROM
        st1
)
SELECT
    st2.id,
    -- 対象書類(jp or ifrs, 府令略号, 様式番号)
    a1[1] AS c1,
    -- 報告書略号
    a1[2] AS c2,
    -- 報告書連番(3桁)
    a1[3] AS c3,
    -- EDINETコード
    a2[1] AS c4,
    -- 追番
    a2[2] AS c5,
    -- 報告対象期末日 or 報告発生義務日
    st2.c3::date AS c6,
    -- 報告書提出回数
    st2.c4 AS c7,
    -- 提出日
    a3[1]::date AS c8,
    -- ファイル拡張子(必要ないかな？)
    a3[2] AS c9,
    -- 名前空間?
    st2.c6 AS c10,
    -- タクソノミ要素名
    st2.c7 AS c11
FROM
    st2;

INSERT INTO tb_egg_processing WITH st1 (
    id,
    a1,
    a2
) AS (
    -- 区切り文字で配列として分割
    SELECT
        id,
        regexp_split_to_array(file_nm, '-') AS c1,
        regexp_split_to_array(element_id, ':') AS c2
    FROM
        tb_egg
    WHERE
        file_nm LIKE 'jpfr%' -- 旧EDINETのタクソノミを除く
),
st2 (
    id, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, a1, c12, c13
) AS (
    SELECT
        st1.id,
        a1[1] AS c1,
        a1[2] AS c2,
        a1[3] AS c3,
        a1[4] AS c4,
        a1[5] AS c5,
        a1[6] AS c6,
        a1[7] AS c7,
        a1[8] AS c8,
        a1[9] AS c9,
        a1[10] AS c10,
        regexp_split_to_array(a1[11], '\.') AS c11,
        a2[1] AS c12,
        a2[2] AS c13
    FROM
        st1
)
SELECT
    st2.id,
    -- 対象書類(jpfr)
    st2.c1 AS c1,
    -- 報告書略号
    st2.c2 AS c2,
    -- 報告書連番(3桁)
    '' AS c3,
    -- EDINETコード
    st2.c3 AS c4,
    -- 追番
    st2.c4 AS c5,
    -- 報告対象期末日 or 報告発生義務日
    (st2.c5 || '-' || st2.c6 || '-' || st2.c7)::date AS c6,
    -- 報告書提出回数
    st2.c8 AS c7,
    -- 提出日
    (st2.c9 || '-' || st2.c10 || '-' || a1[1])::date AS c8,
    -- ファイル拡張子(必要ないかな？)
    a1[2] AS c9,
    -- 名前空間?
    st2.c12 AS c10,
    -- タクソノミ要素名
    st2.c13 AS c11
FROM
    st2;

-- test
SELECT
    report_sequence
FROM
    tb_egg_processing
WHERE
    target_document = 'jpfr'
GROUP BY
    1;

EXPLAIN ANALYZE
SELECT
    file_nm,
    element_id
FROM
    tb_egg
WHERE
    file_nm LIKE 'ifrs%'
    -- GROUP BY
    --     file_nm
LIMIT 10;

select taxonomy_element_name from tb_egg_processing limit 10 ;

-- todo taxonomy_element_nameを集約・日本語名を割り当てたマスタを作成する。
-- 一旦集約したテーブルをcsvで落とし、別ファイルで日本語名とタクソノミ名がセットになったファイルで突き合わせてテーブル保存するかな？
-- 段階的に計画を作成し、徐々に実行していかないとモチベーションが上がらない。。。

-- todo SQLがある程度整備できたら、vuejs, pythonとDjango(python用フレームワーク)でwebアプリ作成開始。設計どうしようかな？？？笑
